% plot sediment logs

clear all;
close all;

rundir2       = 'petm';
datadir2      = 'Sealand/data';
plotdir2      = 'Sealand/plots';

fig1 = figure(100);
set(fig1,'position',[200 100 1000 500],'PaperPositionMode','auto')

fig2 = figure(101);
set(fig2,'position',[200 100 800 400],'PaperPositionMode','auto')

% Physical parameters
lx          = 2000e3;              % basin length
tmax        = 15000;
kappa       = 1e-0;                 % hill-slope diffusion
c           = 1e-1*ones(tmax,1);    % discharge transport coefficient
nexp        = 1;                    % power law exponent of water flux
alpha       = 0.5*ones(tmax,1);     % rainfall rate
alpha       = repmat(alpha,1,4);
for i = 1:4
    alpha(2000:2021,i) = alpha(2000:2021,i) + 0.25*i;
end

% Sea
ksea        = 5e4;
kdecay      = 1e-2*lx;

linecc    = {'k-' 'k--' 'r' 'r--'};
delay    = zeros(4,1);
alphamax = zeros(4,1);

drainsizes = [20 40 80];
for nin = 1:3
    d50 = drainsizes(nin);
    
    figure(fig1)
        
    for mim= 4:-1:1;
        
        input = sprintf('%s/%s/petm_d%dphi%d_fluxhighres_%d.mat',datadir2,rundir2,d50,2*d50,mim);
        load(input);
        time = 1e-6*(time_step:time_step:time_step*tmax);
        
        for t = 1:tmax
            grainpdf(t,1:gfront(t)) = 10.^grainpdf(t,1:gfront(t));
            
            dist = 1e-3*lx*coordinates(t,:);
            grav = grainpdf(t,:);
            grav(eroded(t,:)>0) = NaN;
            
            g50(t)  = mean(grav(dist > 47  & dist < 50));
            g100(t) = mean(grav(dist > 97  & dist < 100));
            g150(t) = mean(grav(dist > 147 & dist < 150));
            g200(t) = mean(grav(dist > 197 & dist < 200));
            g250(t) = mean(grav(dist > 147 & dist < 250));
            g500(t) = mean(grav(dist > 497 & dist < 500));
        end
        
        if (mim== 2)
            qss   = iflux;
            g50s  = g50;
            g100s = g100;
            g150s = g150;
            g200s = g200;
            g500s = g500;
        end
        
        subplot(1,4,1)
        plot(alpha(:,mim),time,linecc{mim}), hold on
        set(gca,'ylim',[9.99 10.4]);
        set(gca,'xlim',[0 1.75]);
        ylabel('Time (Myr)')
        xlabel('Precip. (m/yr)')
                
        subplot(1,4,2)
        plot(g50,time,linecc{mim})
        hold on
        set(gca,'ylim',[9.99 10.4]);
        xlabel('Grain size (mm)')
        ylabel('Time (Myr)')
        title('50 km')
        
        subplot(1,4,3)
        plot(g100,time,linecc{mim})
        hold on
        set(gca,'ylim',[9.99 10.4]);
        xlabel('Grain size (mm)')
        ylabel('Time (Myr)')
        title('100 km')
        
        subplot(1,4,4)
        plot(g500,time,linecc{mim})
        hold on
        set(gca,'ylim',[9.99 10.4]);
        xlabel('Grain size (mm)')
        ylabel('Time (Myr)')
        title('500 km')
        
        [alphamax(mim),indxa] = max(alpha(:,mim));
        
        if any(g50>2.01)
            [~,indxg50]  = find(g50>2.01);
            pdelay50(mim)  = 1e6*(time(indxg50(1))  - 10);
        else
            pdelay50(mim) = NaN;
        end
        if any(g100>2.01)
            [~,indxg100] = find(g100>2.01);
            pdelay100(mim) = 1e6*(time(indxg100(1)) - 10);
        else
            pdelay100(mim) = NaN;
        end
        if any(g150>2.01)
            [~,indxg150] = find(g150>2.01);
            pdelay150(mim) = 1e6*(time(indxg150(1)) - 10);
        else
            pdelay150(mim) = NaN;
        end
        if any(g200>2.01)
            [~,indxg200] = find(g200>2.01);
            pdelay200(mim) = 1e6*(time(indxg200(1)) - 10);
        else
            pdelay200(mim) = NaN;
        end
        if any(g250>2.01)
            [~,indxg250] = find(g250>2.01);
            pdelay250(mim) = 1e6*(time(indxg250(1)) - 10);
        else
            pdelay250(mim) = NaN;
        end
        if any(g500>0.0625)
            [~,indxg500] = find(g500>0.0625);
            pdelay500(mim) = 1e6*(time(indxg500(1)) - 10);
        else
            pdelay500(mim) = NaN;
        end
        
    end
    
    clearvars time g*
    
    figure(fig2)
    
    linecc = {'ok' 'sk' '+k' '^k'};
    for n = 1:4
        plot([50 100 150],1e-3*[pdelay50(n) pdelay100(n) pdelay150(n)],linecc{n})
        hold on
    end
    xlabel('Distance (km)')
    ylabel('Delay (kyr)')
    set(gca,'xlim',[0 200])
    set(gca,'ylim',[0 100])
    legend('0.25 myr^{-1}','0.50 myr^{-1}','0.75 myr^{-1}','1.00 myr^{-1}','location','southeast')
    
    namefig = sprintf('%s/%s/grains_l_d%d.fig',plotdir2,rundir2,d50);
    nameeps = sprintf('%s/%s/grains_l_d%d.eps',plotdir2,rundir2,d50);
    namepng = sprintf('%s/%s/grains_l_d%d.png',plotdir2,rundir2,d50);
    saveas(fig1,namefig,'fig')
    print(fig1,'-depsc',nameeps)
    print(fig1,'-dpng',namepng)
    
    namefig = sprintf('%s/%s/precip_delay_d%d.fig',plotdir2,rundir2,d50);
    nameeps = sprintf('%s/%s/precip_delay_d%d.eps',plotdir2,rundir2,d50);
    namepng = sprintf('%s/%s/precip_delay_d%d.png',plotdir2,rundir2,d50);
    saveas(fig2,namefig,'fig')
    print(fig2,'-depsc',nameeps)
    print(fig2,'-dpng',namepng)
    
    clf(fig1)
    clf(fig2)
    
end