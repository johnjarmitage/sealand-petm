
% 1-D FEM model for sediment transport 

clear all;
close all force;

rundir       = 'petm_d80phi160';

datadir      = 'Sealand/data/petm/';
plotdir      = 'Sealand/plots/';
mkdir(datadir);
mkdir(plotdir,rundir);

fixed_top   = false;
fixed_end   = false;

% Numerical parameters
tmax        = 15000;
nn          = 1000;
X           = linspace(0,1,nn);
X_old       = X;
h           = diff(X);
no_nodes    = nn;
no_el       = no_nodes-1;
no_nodes_el = 2;
lx          = 2000e3;               % basin length
lct         = 100e3;                % catchment length
lxg         = 200e3/lx;
Xg          = linspace(0,lxg,200);
smooth_el   = 15;                   % smooth the diffusion coefficient using a window
% this wide

% Physical parameters
% land
kappa       = 1e-0;                 % hill-slope diffusion
c           = 1e-1*ones(tmax,1);    % discharge transport coefficient
nexp        = 1;                    % power law exponent of water flux
alpha       = 0.5*ones(tmax,1);     % rainfall rate
alpha       = repmat(alpha,1,4);

% make Gaussian curve
% % sigma = 21; %standard deviation in time step width
% % gcoord = linspace(-100,100,201);
% % galpha = (1/(sigma*sqrt(2*pi))) * exp((-(gcoord.*gcoord))./(2*sigma*sigma));
% % for i = 1:4
% %     alpha(2011-100:2011+100,i) = alpha(2011-100:2011+100,i) + galpha'*(0.25/max(galpha))*i;
% % end

% make lognormal curve
sigma  = 0.9; % standard deviation
lcoord = linspace(0.1,50,5000);
lalpha = (1./(lcoord.*sigma*sqrt(2*pi))) .* exp(-((log(lcoord)).*(log(lcoord)))./(2*sigma*sigma));
for i = 1:4
    alpha(10001:15000,i) = alpha(10001:15000,i) + lalpha'*(0.25/max(lalpha))*i;
end

for i = 1:4
    alpha(2000:2021,i) = alpha(2000:2021,i) + 0.25*i;
end

% sea
ksea        = 5e4; %logspace(4,5,10); %logspace(4,5,10); % 5e4; % diffusion for the sea
kdecay      = 1e-2*lx; %logspace(-3,-1,10)*lx; % decay coefficient as wave control reduces with depth

kappa_eff   = kappa + c(1)*(alpha(1,1)*lct)^nexp;

% name5 = sprintf('%s/%s_K%.1e_c%.1e_n%d_alpha%.1e_ksea%.1e.mat',...
%     datadir,rundir,kappa,c(1),nexp,alpha(1));
% name5 = sprintf('%s/%s_K%.1e_c%.1e_n%d_alpha%.1e_ksea%.1e_ksea%.1e.mat',...
%     datadir,rundir,kappa,c(1),nexp,alpha(1),ksea);
% name5 = sprintf('%s/%s_K%.1e_c%.1e_n%d_alpha%.1e_ksea%.1e_kdecay%.1e.mat',...
%     datadir,rundir,kappa,c(1),nexp,alpha(1),ksea,kdecay);

% time step
time_step   = 1000; %yrs
dt          = time_step*kappa/(lx*lx);

% Flux input of sediment
% iflux       = zeros(tmax,1);
iflux         = (40/kappa)*ones(tmax,1);         % m^2/yr
% periodicity = 1;    % millions of years
% iflux       = (50/kappa) + (5/kappa)*sin(periodicity*2e-6*pi*(time_step:time_step:time_step*tmax));
% iflux       = iflux';
% iflux  = repmat(iflux,1,4);
% liflux = (1./(lcoord.*sigma*sqrt(2*pi))) .* exp(-((log(lcoord)).*(log(lcoord)))./(2*sigma*sigma));
% for i = 1:4
%     iflux(10001:15000,i) = iflux(10001:15000,i) + liflux'*(10/max(liflux))*i;
% end


% sea level
coastlength   = 10e3/lx;
seamag        = 0;
sealand       = seamag/lx*ones(tmax,1);
% periodicity   = 1/2.0;    % 1/millions of years
% sealand       = seamag/lx + 1e1/lx*sin(periodicity*2e-6*pi*(time_step:time_step:time_step*tmax));

% sinusoidal rain
% periodicity   = 1/0.1;    % 1/millions of years
% for n = 1:10
%     alpha(:,n) =  alpha(1,n) + 0.5*alpha(1,n)*sin(periodicity*2e-6*pi*(time_step:time_step:time_step*tmax)-2*periodicity*pi);
% end

% change precipitation during time intervals
% for n = 1:10
%     alpha(500:780,n) = 10*alpha(500:780,n);
% end

% selective deposition
% log normal...
gfrac       = 0.05;                      % gravel fraction in supply
% gfracn      = linspace(.05,0.5,10);
lambda      = 0.0;                       % porosity
grainpdf0   = log10(80);                 % initial grain size pdf log(D50)
phi0        = log10(2)*ones(1,tmax); % standard deviation of the sediment supply log(D84/D50)
sandpdf0    = 2;                         % initial sand grain size
C1          = 0.75;                      % Fedele & Paola 2007 / Duller et al., 2010
C3          = 0.95;
Cv          = 1;
Cmult       = 1/Cv;

% undefined distribution
% lambda        = 0.3;
% grainpdf0     = 0.05;
% phi0          = 0.05*ones(1,tmax);
% phi_halflife  = .5e6;  % yrs
% decay         = lx*lx*dt*log(.5)/(kappa*phi_halflife);
% phi0(251:end) = 0.075 + (0.05 - 0.075).*exp(decay*(1:250));
% C1            = 0.75;
% Cv            = 1.2;uplift     = -uamp*lx/kappa*ones(size(X))';

% Cmult         = 1/Cv;

% Initialization of matrixes and vectors
KM              = zeros(no_nodes,no_nodes);
MM              = zeros(no_nodes,no_nodes);
F               = zeros(no_nodes,1);
Z               = zeros(no_nodes,1);
Z_old           = zeros(no_nodes,1);
nodes           = zeros(no_el,2);

elevation       = zeros(tmax,no_nodes); % topography across the system by timestep
eroded          = zeros(tmax,no_nodes);
deposit         = zeros(tmax,no_nodes);
coordinates     = zeros(tmax,no_nodes);
fan_elevation   = zeros(tmax,no_nodes);
fan_uplift      = zeros(tmax,no_nodes);

qsfine          = zeros(tmax,no_nodes); % qs across the basin
y               = zeros(tmax,no_nodes); % variable for down stream fining
chi             = zeros(tmax,no_nodes);
grainpdf        = zeros(tmax,no_nodes); % mean grain size
% grainpdf_FP07   = zeros(tmax,no_nodes);

gfront          = zeros(tmax,1);
hr_gfront       = zeros(tmax,1);
sed_acc         = zeros(tmax,1);
coastline       = zeros(tmax,1);

% initial condition
slope           = -5/kappa_eff;
Zleft           = -10e3*slope/lx;
Zright          = -100/lx;
% Z             = Zleft*ones(no_nodes,1)/lx;
% Z             = ((1e3/lx)*sin(pi*X + pi))';
Z               = slope.*X' + Zleft;
Z(Z<Zright)     = Zright-400/lx;
Zinitial        = Z;

% xq              = linspace(0,lx,no_nodes);
% zerop           = 0.3*lx; %
% zerod           = 5;
% sedT            = load('Bookcliffs/T0.csv');
% Z               = -interp1([sedT(:,1); zerop; lx],[sedT(:,2); zerod; zerod],xq,'pchip')'./lx;
% Zleft           = Z(1);
% Zright          = Z(end);
% Zinitial        = Z;

% subsidence (I call it uplift)
H          = 1;        % on/off
uamp       = .3e-4;    % m/yr
% uplift     = -uamp*lx/kappa*ones(size(X))';
% uampM    = 2;
% uplift   = zeros(size(X))';
% uplift     = linspace(0,-uamp*lx/kappa,length(X))';
halfdist   = log(.02)/0.5;
uplift     = -uamp*lx/kappa*exp(halfdist*X') - 0.5*uamp;
% uplift   = ((uamp*lx/kappa)*(sin(pi*.5*X + pi)))';
% uplift   = ((uamp*lx/kappa)*(sin(3*pi*X + .5*pi)))' +...
%     linspace(-uamp*lx/kappa,-uampM*uamp*lx/kappa,length(X))';
uplift_old = uplift;

% Node numbering
for i=1:no_el
    iel          = i;
    nodes(iel,1) = i;
    nodes(iel,2) = i+1;
end

% h1 = waitbar(0,'This will take some time...');
nflux = 1;
for nalpha = 1:4
    waitstring = sprintf('nalpha = %d, lets do this...',...
        nalpha);
    disp(waitstring);
    % gfrac = gfracn(nksea);
    % h2 = waitbar(0,waitstring);
    for t =1:tmax
        
        % uplift = load_escanilla(t,time_step,X,lx,kappa,no_nodes);
        % [uplift_old,iflux(t),gfrac] = load_bookcliffs(t,time_step,X_old,lx,kappa,no_nodes);
        
        if t == 1
            Z = Zinitial;
        end
        
        Z_old = Z;
        De    = c(t)*(alpha(t,nalpha)*lx)^nexp/kappa;
        D     = zeros(1,no_el);
        
        % find the coast from previous time step to change resolution
        % here
        cn    = find(Z_old > sealand(t));
        if isempty(cn)
            coastline(t) = 1;
        else
            coastline(t) = cn(end);
            clearvars cn
            
            % change model resolution at the coast (keep number of nodes
            % the same)
            
            nnc         = 100;                  % new highres coastline
            nnn         = nn - nnc;             % number of nodes for the left coordinates
            if (coastline(t) < 6)
                pintcr      = X_old(40);
                pintcr1     = pintcr + (1-pintcr)/nnn;
                X           = [linspace(0,pintcr,nnc) linspace(pintcr1,1,nnn)];
            elseif (coastline(t) > 5 && no_nodes-coastline(t) > 36)
                splitnn     = X_old(coastline(t));      % need to find where to put the high resolution coastline in
                nnhleft     = round(nnn*splitnn);
                nnhright    = nnn - nnhleft;
                pintcl      = X_old(coastline(t)-4);    % take -4 nodes left of caostline
                pintcr      = X_old(coastline(t)+36);   % take +36 nodes left of caostline
                pintcl1     = pintcl + (pintcr-pintcl)/nnc;
                pintcr1     = pintcr + (1-pintcr)/nnhright;
                X           = [linspace(0,pintcl,nnhleft) linspace(pintcl1,pintcr,nnc) linspace(pintcr1,1,nnhright)];
            else
                pintcl      = X_old(no_nodes-40);
                pintcl1     = pintcl + (1-pintcl)/nnn;
                X           = [linspace(0,pintcl,nnn) linspace(pintcl1,1,nnc)];
            end
            h           = diff(X);
            Z_interp    = interp1(X_old,Z_old,X,'pchip')';
            uplift      = interp1(X_old,uplift_old,X,'pchip')';
            
            Z     = solve_sealand(De,nexp,lct,ksea,kdecay,no_el,nodes,no_nodes,...
                no_nodes_el,lx,h,dt,fixed_top,fixed_end,Zleft,Zright,H,X,...
                Z_interp,t,smooth_el,coastline(t),sealand,uplift,iflux);
            
            % re-find the coast for the new coordinate system
            cn    = find(Z > sealand(t));
            if isempty(cn)
                coastline(t) = 1;
            else
                coastline(t) = cn(end);
            end
        end
        clearvars cn
        
        % save stuff for plotting
        coordinates(t,:)            = X;
        elevation(t,:)              = interp1(X,Z,X_old,'pchip')';
        fan_elevation(t,:)          = elevation(t,:);
        fan_uplift(t,:)             = uplift_old;
        
        % grain sizes on uniform grid
        difference                  = Z_old + dt*uplift_old - elevation(t,:)';
        deposit_now                 = zeros(1,no_nodes);
        eroded(t,difference>0)      = difference(difference>0);
        
        deposit_now(difference<=0)  = -difference(difference<=0);
        deposit_now(difference>0)   = 0;
        deposit(t,:)                = deposit_now;
        sed_acc(t)                  = trapz(deposit_now)/dt;
        
        if t ~= 1
            for f=1:t-1
                fan_elevation(f,fan_elevation(f,:)>=elevation(t,:)) = elevation(t,fan_elevation(f,:)>=elevation(t,:));
                fan_elevation(f,:) = fan_elevation(f,:) + dt*fan_uplift(t,:);
            end
        end
        
        % grain size fining and gravel fronts
        
        gpdf                        = zeros(size(X_old));
        qsfine(t,:)                 = iflux(t,nflux)*dt + trapz(X_old,eroded(t,:)) - cumtrapz(X_old,deposit_now);
        if gfrac < 1
            % also interpolate to get more smooth gravel front...
            hr_qsf                  = interp1(X_old,qsfine(t,:),Xg,'pchip');
            % hr_qsf(hr_qsf<1e-11)    = NaN; % not too small please
        end
        % qsfine(t,qsfine(t,:)<1e-11) = NaN; % not too small please
        chi(t,:)                    = cumtrapz(X_old,deposit_now)./(iflux(t,nflux)*dt);
        
        gefflux = (1-gfrac)*(iflux(t,nflux)*dt + trapz(X_old,eroded(t,:)));
        if gfrac < 1
            gn = find(qsfine(t,:) > gefflux);
            if isempty(gn)
                gfront(t) = 0;
            else
                gfront(t) = gn(end);
            end
            if gfront(t) > 1
                yg                  = ((1-lambda)/gfrac).*cumtrapz(X_old(1:gfront(t)),deposit_now(1:gfront(t))./qsfine(t,1:gfront(t)));
                yg(yg<0)            = NaN;
                gpdf(1:gfront(t))   = grainpdf0 + phi0(t)*Cmult*(exp(-C1*yg) - 1);
                if X_old(gfront(t)) < lxg
                    hr_gn           = find(hr_qsf > gefflux);
                    hr_gfront(t)    = Xg(hr_gn(end));
                else
                    hr_gfront(t)    = X_old(gfront(t));
                end
            end
            ys                      = ((1-lambda)/(1-gfrac)).*cumtrapz(X_old(gfront(t)+1:end),deposit_now(gfront(t)+1:end)./qsfine(t,gfront(t)+1:end));
            ys(ys<0)                = NaN;
            gpdf(gfront(t)+1:end)   = sandpdf0*exp(-C3*ys);
        else
            y(t,:)                  = (1-lambda)*cumtrapz(X_old,deposit_now./qsfine(t,:));
            y(t,qsfine(t,:)<0)      = NaN;
            gpdf(:)                 = grainpdf0 + phi0(t)*Cmult*(exp(-C1*y(t,:)) - 1);
        end
        gpdf(isnan(gpdf))           = 0;
        
        clearvars deposit_now eroded_now
        
        % lower resolution back to coarse grid
        Z             = elevation(t,:)';
        grainpdf(t,:) = gpdf;
        
        % waitbar(t/tmax,h2)
    end
    % close(h2)
    % name5 = sprintf('%sprecip_%s_K%.1e_c%.1e_n%d_alpha%.1e_ksea%.1e_kdecay%.1e.mat',...
    %     datadir,rundir,kappa,c(1),nexp,alpha(1,nalpha),ksea,kdecay);
    name5 = sprintf('%s%s_fluxhighres_%d.mat',...
        datadir,rundir,nalpha);
    save(name5);
    
    % waitbar(nalpha/10,h1)
end

% close(h1)
% exit;
