function Z = solve_sealand( De,nexp,lct,ksea,kdecay,no_el,nodes,no_nodes,...
    no_nodes_el,lx,h,dt,fixed_top,fixed_end,Zleft,Zright,H,X,Z,t,...
    smooth,sea,sealand,uplift,iflux)
%SOLVE_SEALAND Solve for both sea and land
%   Change diffusion coefficeint and law if below the sea

% Setup system matrix
KM          = zeros(no_nodes,no_nodes);
MM          = zeros(no_nodes,no_nodes);
F           = zeros(no_nodes,1);
D           = zeros(no_el,1);

% calculate the diffusion coefficeint
for iel=1:no_el
    Xmid       = X(iel)+.5*h(iel);
    % sea or land
    if (iel >= sea)
        %D(iel) = ksea;                  % linear diffusion for the sea
        D(iel) = ksea*exp(-kdecay*abs(sealand(t)-.5*(Z(iel)+Z(iel+1))));
    else
        D(iel) = 1 + De*((Xmid+lct/lx).^nexp);
        % assume water runs left to right
        % assume we are in the basin
    end
end

% smooth (using a moving average) the diffusion coefficient...
for iel= 1:no_el
    if iel>smooth && iel<no_el-smooth
        D(iel) = sum(D(iel-smooth:iel+smooth)'.*h(iel-smooth:iel+smooth))/...
            sum(h(iel-smooth:iel+smooth));
    end
end

% solve
for iel=1:no_el
    % Setup element matrix
    F_loc      = H*[h(iel)/2 h(iel)/2];  % Right hand side vector
    MM_loc     = h(iel)*[1/3 1/6;1/6 1/3];
  
    KM_loc  = D(iel)*[1/h(iel) -1/h(iel); -1/h(iel) 1/h(iel)];
    for i=1:no_nodes_el
        ii  = nodes(iel,i);
        for j=1:no_nodes_el
            jj        = nodes(iel,j);
            KM(ii,jj) = KM(ii,jj) + KM_loc(i,j);
            MM(ii,jj) = MM(ii,jj) + MM_loc(i,j);
        end
        F(ii)         = F(ii)     + F_loc(i)*uplift(ii); %??
    end
end

F_tot                =       F   + 1/dt*MM*Z;
KK_TOT               = 1/dt*MM   +      KM;

if logical(fixed_top) && logical(fixed_end) == 1
    Free               = 1:no_nodes;
    Free([1 no_nodes]) = [];
    Z                  = zeros(no_nodes,1);
    Z([1 no_nodes])    = [Zleft/lx Zright/lx];
    
    %flux in boundary
    F_tot(1)         = F_tot(1) + iflux(t);
    F_tot            = F_tot - KK_TOT*Z;
    
    U                = chol(KK_TOT(Free,Free));
    L                = U';
    Z(Free)          = U\(L\F_tot(Free));
    
elseif logical(fixed_top) == 1
    Free             = 1:no_nodes;
    Free([1])        = [];
    Z                = zeros(no_nodes,1);
    Z([1])           = [Zleft/lx];
    
    %flux in boundary
    F_tot(1)         = F_tot(1) + iflux(t);
    F_tot            = F_tot - KK_TOT*Z;
    
    U                = chol(KK_TOT(Free,Free));
    L                = U';
    Z(Free)          = U\(L\F_tot(Free));
    
elseif logical(fixed_end) == 1
    Free             = 1:no_nodes;
    Free([no_nodes]) = [];
    Z                = zeros(no_nodes,1);
    Z([no_nodes])    = [Zright/lx];
    
    %flux in boundary
    F_tot(1)         = F_tot(1) + iflux(t);
    F_tot            = F_tot - KK_TOT*Z;
    
    U                = chol(KK_TOT(Free,Free));
    L                = U';
    Z(Free)          = U\(L\F_tot(Free));
    
else
    U                = chol(KK_TOT);
    L                = U';
    
    % flux in boundary
    F_tot(1)         = F_tot(1) + iflux(t);
    Z                = U\(L\F_tot);
end
% Free memory
clear KM MM F F_tot KK_TOT D
end

