function Z = solve_land(De,nexp,lct,no_el,nodes,no_nodes,no_nodes_el,...
    lx,h,dt,fixed_top,fixed_end,Zleft,Zright,H,X,Z,t,uplift,iflux)
%SOLVE_LAND Solve for diffusion without sea
%   Solves the lenth dependent diffusion equation assuming no sea to get a
%   first sweep at elevation

% Setup system matrix
KM          = zeros(no_nodes,no_nodes);
MM          = zeros(no_nodes,no_nodes);
F           = zeros(no_nodes,1);
D           = zeros(no_el,1);

for iel=1:no_el
    
    % Setup element matrix
    F_loc      = H*[h(iel)/2 h(iel)/2];  % Right hand side vector
    MM_loc     = h(iel)*[1/3 1/6;1/6 1/3];
    Xmid       = X(iel)+.5*h(iel);
    
    D(iel) = 1 + De*((Xmid+lct/lx).^nexp); % assume water runs left to right
    
    KM_loc  = D(iel)*[1/h(iel) -1/h(iel); -1/h(iel) 1/h(iel)];
    for i=1:no_nodes_el
        ii  = nodes(iel,i);
        for j=1:no_nodes_el
            jj        = nodes(iel,j);
            KM(ii,jj) = KM(ii,jj) + KM_loc(i,j);
            MM(ii,jj) = MM(ii,jj) + MM_loc(i,j);
        end
        F(ii)         = F(ii)     + F_loc(i)*uplift(ii); %??
    end
end

F_tot                =       F   + 1/dt*MM*Z;
KK_TOT               = 1/dt*MM   +      KM;

if logical(fixed_top) && logical(fixed_end) == 1
    Free               = 1:no_nodes;
    Free([1 no_nodes]) = [];
    Z                  = zeros(no_nodes,1);
    Z([1 no_nodes])    = [Zleft/lx Zright/lx];
    
    %flux in boundary
    F_tot(1)         = F_tot(1) + iflux(t);
    F_tot            = F_tot - KK_TOT*Z;
    
    U                = chol(KK_TOT(Free,Free));
    L                = U';
    Z(Free)          = U\(L\F_tot(Free));
    
elseif logical(fixed_top) == 1
    Free             = 1:no_nodes;
    Free([1])        = [];
    Z                = zeros(no_nodes,1);
    Z([1])           = [Zleft/lx];
    
    %flux in boundary
    F_tot(1)         = F_tot(1) + iflux(t);
    F_tot            = F_tot - KK_TOT*Z;
    
    U                = chol(KK_TOT(Free,Free));
    L                = U';
    Z(Free)          = U\(L\F_tot(Free));
    
elseif logical(fixed_end) == 1
    Free             = 1:no_nodes;
    Free([no_nodes]) = [];
    Z                = zeros(no_nodes,1);
    Z([no_nodes])    = [Zright/lx];
    
    %flux in boundary
    F_tot(1)         = F_tot(1) + iflux(t);
    F_tot            = F_tot - KK_TOT*Z;
    
    U                = chol(KK_TOT(Free,Free));
    L                = U';
    Z(Free)          = U\(L\F_tot(Free));
    
else
    U                = chol(KK_TOT);
    L                = U';
    
    % flux in boundary
    F_tot(1)         = F_tot(1) + iflux(t);
    Z                = U\(L\F_tot);
end
% Free memory
clear KM MM F F_tot KK_TOT D
end

